

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user.model';
import { LoginService } from '../login.service';
import {NgForm} from '@angular/forms';
import { ToasterService } from './../toaster-service.service';
import { ReactiveFormsModule, FormsModule,  FormGroup,  FormControl,  Validators,  FormBuilder} from '@angular/forms';


declare var $: any;
declare var jQuery: any;
//declare var remove_error_msg(): any;
var btnDisabled: boolean;
 btnDisabled = true;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
//   providers: [LoginServices]
})
export class FooterComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ]),
    subject:  new FormControl('', Validators.required),
    message:  new FormControl('', Validators.required),
  
  });
  
    
    
  showMessage : boolean = true;
  submitted: string;
  MyClass: any;

  

  name: string = "";
  email: string = "";
  subject: string = "";
  message: string = "";

  constructor(public router: Router, public _loginService: LoginService, private toasterService: ToasterService) { 

    
  }


 
         

  Success(){
    this.toasterService.Success("Your message has been sent successfully.")
  }
  Info(){
    this.toasterService.Info("Subscribed")
  }
  ngOnInit() {
    $(document).ready(function(){
        $('.modal').modal();
      });

    $(document).ready(function() {
      $(".position5").hide();
      $(".position2").show();
      $('.position2').click(function() {
       $(".position5").slideToggle();
      });
  });
  
  $(".waves-circle i").click(function() {
    $(this).text(function(i, text) {
        return text === "close" ? "comment" : "close"
    })
});
(function($) {
    $('div.whatsappme__button1').click(function() {
        $('div.whatsappme__box1').show()
    });
    $('#message_close').click(function() {
        $('.whatsappme__box1').hide()
    })
}(jQuery));

(function ($) {
  "use strict";

  /*==================================================================
  [ Focus Contact2 ]*/
  $('.input100').each(function(){
      $(this).on('blur', function(){
          if($(this).val().trim() != "") {
              $(this).addClass('has-val');
          }
          else {
              $(this).removeClass('has-val');
          }
      })    
  })

  /*==================================================================
  [ Validate ]*/
  var input = $('.validate-input .input100');

  $('.validate-form').on('submit',function(){
      var check = true;

      for(var i=0; i<input.length; i++) {
          if(validate(input[i]) == false){
              showValidate(input[i]);
              check=false;
          }
      }

      return check;
  });


  $('.validate-form .input100').each(function(){
      $(this).focus(function(){
         hideValidate(this);
      });
  });

  function validate (input) {
      if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
          if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
              return false;
          }
      }
      else {
          if($(input).val().trim() == ''){
              return false;
          }
      }
  }

  function showValidate(input) {
      var thisAlert = $(input).parent();

      $(thisAlert).addClass('alert-validate');
  }

  function hideValidate(input) {
      var thisAlert = $(input).parent();

      $(thisAlert).removeClass('alert-validate');
  }
  

})(jQuery);

  }

  myFunction() {
      
//  var myButtonClasses = document.getElementById("div1").classList;
//  myButtonClasses.remove("red123");
//  myButtonClasses.add("blue123");

setTimeout(function() {

$('#ex1').find("*").removeClass("alert-validate");




    console.log("okkkkk");


}, 10);

  }

 login() {
  console.log();
  let user = new User('', '', '', '');
  user.name = this.form.value.name;
  user.email = this.form.value.email;
  user.subject = this.form.value.subject;
  user.message = this.form.value.message;
  let data = [{
      'name': user.name,
      'email': user.email,
      'subject': user.subject,
      'message': user.message
  }];

  console.log(data);
  
  if( this.form.value.name!="" && this.form.value.email!="" && this.form.value.subject!="" && this.form.value.message!="" &&this.form.value.name!=null && this.form.value.email!=null && this.form.value.subject!=null && this.form.value.message!=null   ){
  this._loginService.sendLogin({
      data
  }).subscribe(
    response => this.handleResponse(response),
    error => this.handleResponse(error),
     );
 this.showMessage = false;
 

 this.form.reset();
 this.myFunction();
   this.Success();
      
}
}

remove_error_msg(){
  $("#wrap-input100").removeClass("alert-validate");

};
// rmove_bloker_class()

  // onSubmit(){
  
  //     console.log("submitted");
  //     this.myform.reset()
 
   
  // }
  handleResponse(response) {
    if (response.success) {
      console.log("success");
    } else if (response.error) {
      console.log("errror");
    } else {

    }

  }

}
