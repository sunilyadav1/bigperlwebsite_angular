import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WindowsAppDevelopmentComponent } from './windows-app-development.component';

describe('WindowsAppDevelopmentComponent', () => {
  let component: WindowsAppDevelopmentComponent;
  let fixture: ComponentFixture<WindowsAppDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WindowsAppDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WindowsAppDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
