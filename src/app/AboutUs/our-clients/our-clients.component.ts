import { Component, OnInit } from '@angular/core';
declare var  $: any;
declare var jQuery: any;
@Component({
  selector: 'app-our-clients',
  templateUrl: './our-clients.component.html',
  styleUrls: ['./our-clients.component.css']
})
export class OurClientsComponent implements OnInit {

  constructor() { }

  ngOnInit() {


    $('#customers-carousel3').owlCarousel({responsive:{0:{items:1},600:{items:3},1000:{items:4}},loop:true,margin:30,nav:true,smartSpeed:2000,navText:["<i class='fa fa-chevron-left position'></i>","<i class='fa fa-chevron-right position1'></i>"],autoplay:true,autoplayTimeout:3000});
  
  
    (function ($) {
      "use strict";
  
      /*==================================================================
      [ Focus Contact2 ]*/
      $('.input100').each(function(){
          $(this).on('blur', function(){
              if($(this).val().trim() != "") {
                  $(this).addClass('has-val');
              }
              else {
                  $(this).removeClass('has-val');
              }
          })    
      })
  
      /*==================================================================
      [ Validate ]*/
      var input = $('.validate-input .input100');
  
      $('.validate-form').on('submit',function(){
          var check = true;
  
          for(var i=0; i<input.length; i++) {
              if(validate(input[i]) == false){
                  showValidate(input[i]);
                  check=false;
              }
          }
  
          return check;
      });
  
  
      $('.validate-form .input100').each(function(){
          $(this).focus(function(){
             hideValidate(this);
          });
      });
  
      function validate (input) {
          if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
              if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                  return false;
              }
          }
          else {
              if($(input).val().trim() == ''){
                  return false;
              }
          }
      }
  
      function showValidate(input) {
          var thisAlert = $(input).parent();
  
          $(thisAlert).addClass('alert-validate');
      }
  
      function hideValidate(input) {
          var thisAlert = $(input).parent();
  
          $(thisAlert).removeClass('alert-validate');
      }
      
  
  })(jQuery);
  
  
  
  }

}
